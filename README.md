# Student Connect #


### What is this repository for? ###

* Student Connect is a communication app aimed at allowing Instructors at BCIT to have a single platform to communicated pertinent course announcements to their students. Student Connect allows users to create groups and send push notifications to announce postings to the groups. 

* Version 1.0

